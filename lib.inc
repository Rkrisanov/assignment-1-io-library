section .data

section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall

string_length:; Принимает указатель на нуль-терминированную строку, возвращает её длину:

    mov rax,0
    .cycle:
        cmp byte [rax+rdi],0
        jz .ret_string_lenght
        inc rax
        jmp .cycle
    .ret_string_lenght:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rdi
    mov rdx,rax ; string length in bytes
    mov rsi, rdi ; string address

    mov rax, 1 ; 'write' syscall number
    mov rdi, 1 ; stdout descriptor
    syscall
    ret

; Принимает код символа и выводит его в stdout
; changed rdi,rdx,rsi,rax
print_char:
    push 0
    mov [rsp],rdi ; 'write' char
    mov rdx, 1 ; string length in bytes
    mov rsi, rsp ; string address
    mov rax, 1 ; 'write' syscall number
    mov rdi, 1 ; stdout descriptor
    syscall
    pop rax
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi,'\n'
	call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:

    mov rax, rdi
    mov r8, rsp
    mov r9, 10 ; the base of the number system
    dec rsp
    mov byte[rsp],0 ; null-terminated char in end of string

    .cycle  dec rsp

            mov rdx,0
            div r9
            mov byte[rsp],'0'
            add byte[rsp],dl


            cmp rax,0
            jnz .cycle

    .print  mov rdi,rsp
            call print_string
            mov rsp,r8
            ret


; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi,rdi
    jns print_uint

    push rdi
    mov rdi,'-'
    call print_char
    pop rdi
    neg rdi
    jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    mov rax, 1
    xor rcx, rcx
    .cycle
    mov r8b,byte[rdi+rcx]
    mov r9b,byte[rsi+rcx]
    cmp r8b,r9b
    jne .nequal
    
    cmp r8b,0
    je .equal
    
    inc rcx 
    
    jmp .cycle
    .nequal
    mov rax,0
    .equal
    ret

read_char: 
    xor rax,rax 
    xor rdi,rdi ; 0 - stdin descriptor
    mov rdx,1 ; string length in bytes
    push 0
    mov rsi,rsp ; place where the read char would be
    syscall 

    pop rax
    ret 



;received char in rdi
; return 1 in rax if char is whitespace and 0 if else
is_whitespace_char:
    cmp rdi, ' '
    je .is_whitespace
    cmp rdi, '\n'
    je .is_whitespace
    cmp rdi, `\t`
    je .is_whitespace

    mov rax,0
    ret 
    .is_whitespace:
        mov rax,1
        ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
;адрес начала буфера - rdi
;размер буфера - rsi
read_word: ;magnum opus
    push rbx
    mov r8, rsi
    xor rcx, rcx
    mov rdx, 1
    mov r10, 0 ; flag signalising recording word started
    ; I used finite state machines in this
    ; r10 and r9 responsible for 4 state of subroutite
    ; r10 - starts record word into memory
    ; r9  - flags of unrecordable chars
    ; this flags checks after reading another char
    ; r10, r9
    ; 0  , 0 - subroutite starts record word into memory
    ; 0  , 1 - read char again until meet recordable char
    ; 1  , 0 - ends executing
    ; 1  , 0 - read again and record chars
    ; 
    .reading_char:

        push rdi
        push rdx
        push rsi
        push rcx
        call read_char
        pop rcx
        pop rsi
        pop rdx
        pop rdi

        cmp rax, 0
        je .success

        push rdi
        push rax
        mov rdi,rax
        call is_whitespace_char
        mov r9,rax
        pop rax
        pop rdi

        cmp r9,1 ; if char is whitespace
        jne .border_of_word
        jmp .check_end
    .check_end
        cmp r10,1
        je .success
        jmp .reading_char
    .border_of_word
        mov r10,1
        mov [rdi+rcx],rax
        inc rcx
        cmp r8, rcx 
        jbe .wrong
        jmp .reading_char
    .success: 
        mov byte[rdi+rcx], 0
        mov rdx,rcx
        mov rax, rdi
        jmp .exit
    .wrong:
        mov rdx,rcx 
        xor rax, rax 
    .exit:
        pop rbx
        ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rcx, rcx
    xor r8, r8
    mov r9, 0xA
    .parsing:
        mov r8b,byte[rdi+rcx]
        cmp r8,0
        je .exit

        sub r8,'0'
        
        cmp r8,9
        ja .exit
        mul r9

        add rax,r8

        inc rcx
        jne .parsing

    .fail
    .exit
        mov rdx,rcx
        ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    xor rcx, rcx
    xor rdx,rdx
    xor r8, r8

    .sign_process
        mov r8b,byte[rdi]
        cmp r8,'-'
        je .pastring_negative
    .parsing_positive
        call parse_uint
        cmp rdx,0
        je .failed
        ret
    .pastring_negative
        inc rdi
        call parse_uint
        cmp rdx,0
        je .failed
        neg rax
        inc rdx
        ret
    .failed
        ret
; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    ; rdi,rsi,rdx
    xor rax, rax
    xor rcx,rcx

    .coping
    mov rax,[rdi+rcx]
    cmp rax,0
    je .exit
    mov [rsi+rcx],rax
    inc rcx
    cmp rcx,rdx
    ja .fail
    jmp .coping


    .fail
    mov rax,0
    ret
    .exit
    mov rax,rcx
    ret
